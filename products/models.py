from django.db import models
from django.contrib.auth import get_user_model

from common.models import TimeStampedModel


User = get_user_model()


class Product(TimeStampedModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    image = models.ImageField(upload_to="product_images", null=True, blank=True)


class ProductStock(TimeStampedModel):
    product = models.ForeignKey(
        Product, on_delete=models.CASCADE, related_name="stocks"
    )
    size = models.CharField(max_length=20, null=True, blank=True)
    initial_amount = models.IntegerField()


class ProductEntity(TimeStampedModel):
    stock = models.ForeignKey(
        ProductStock, on_delete=models.CASCADE, related_name="entities"
    )
    bar_code = models.CharField(max_length=100)


class ProductImage(TimeStampedModel):
    product = models.ForeignKey(
        Product, on_delete=models.CASCADE, related_name="images"
    )
    image = models.ImageField(upload_to="product_images")
    order = models.IntegerField()
