from django.test import TestCase
from django.test.selenium import SeleniumTestCase, LiveServerTestCase


class BaseTestCase(TestCase):
    fixtures = [""]

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
