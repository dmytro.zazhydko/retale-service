from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions

from products.models import Product, ProductStock, ProductEntity, ProductImage
from products.serializers import (
    ProductSerializer,
    ProductStockSerializer,
    ProductEntitySerializer,
)


class ProductViewSet(ModelViewSet):
    serializer_class = ProductSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    queryset = Product.objects.all()


class ProductStockViewSet(ModelViewSet):
    serializer_class = ProductStockSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    queryset = ProductStock.objects.all()


class ProductEntityViewSet(ModelViewSet):
    serializer_class = ProductEntitySerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    queryset = ProductEntity.objects.all()


class ProductImageSet(ModelViewSet):
    serializer_class = ProductImageSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    queryset = ProductImage.objects.all()
