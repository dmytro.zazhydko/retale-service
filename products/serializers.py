from rest_framework.serializers import ModelSerializer
from products.models import Product, ProductStock, ProductEntity, ProductImage


class ProductSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = "__all__"


class ProductStockSerializer(ModelSerializer):
    class Meta:
        model = ProductStock
        fields = "__all__"


class ProductEntitySerializer(ModelSerializer):
    class Meta:
        model = ProductEntity
        fields = "__all__"


class ProductImageSerializer(ModelSerializer):

    class Meta:
        mopdel = ProductImage
        fields = "__all__"
