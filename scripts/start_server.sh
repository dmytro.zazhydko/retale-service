set -v
# report fails early
set -e
cd /code
# Sleep until DB is up and running
sleep 5
export DJANGO_SETTINGS_MODULE=my_pdp.settings
python manage.py migrate --noinput
# python manage.py runserver 0.0.0.0:8001 --nothreading
gunicorn --worker-class gthread --threads 2 --workers 2 --bind 0.0.0.0:8001 my_pdp.wsgi --reload
