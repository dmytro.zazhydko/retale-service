FROM python:3.10.9-slim-bullseye

RUN apt-get update && apt-get install --no-install-recommends -y  \
    build-essential \
    # psycopg2 dependencies
    libpq-dev \
    git

RUN apt-get update && apt-get install --no-install-recommends -y \
    # psycopg2 dependencies
    libpq-dev \
    # playwright dependencies
    libglib2.0-0 libnss3 libnspr4 libatk1.0-0 libatk-bridge2.0-0 libcups2 \
    libdrm2 libdbus-1-3 libxcb1 libxkbcommon0 libx11-6 libxcomposite1 \
    libxdamage1 libxext6 libxfixes3 libxrandr2 libgbm1 libpango-1.0-0 \
    libcairo2 libasound2 libatspi2.0-0 libwayland-client0 \
    # pdf parsing
    poppler-utils\
    # html purify
    npm nodejs \
    && rm -rf /var/lib/apt/lists/*
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

ARG APP_HOME=/code
WORKDIR ${APP_HOME}

COPY . ${APP_HOME}
RUN pip install -r requirements.txt
