from django.db import models
from django.contrib.auth import get_user_model

from common.models import TimeStampedModel

User = get_user_model()


# TODO: add more usefull fields
class UserProfile(TimeStampedModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
