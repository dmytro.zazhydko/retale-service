from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model

User = get_user_model()


class CreateUserTestCase(TestCase):
    def setUp(self) -> None:
        username = "d1masik"
        email = "dima13za@gmail.com"
        password = "MyPassword123!"

        self.create_user_payload = {
            "username": username,
            "email": email,
            "password": password,
        }
        return super().setUp()

    def test_create_user_positive(self):
        payload = self.create_user_payload.copy()
        response = self.client.post(reverse("register"), data=payload)
        user = User.objects.filter(username=payload["username"]).first()
        self.assertTrue(user.check_password(payload["password"]))
        self.assertEqual(response.status_code, 201)

    def test_create_user_unique_checks(self):
        payload = self.create_user_payload.copy()
        response = self.client.post(reverse("register"), data=payload)
        self.assertEqual(response.status_code, 201)
        payload["username"] = "dimasik"
        response = self.client.post(reverse("register"), data=payload)
        self.assertEqual(response.status_code, 400)
        payload["email"] = "dima131za@gmail.com"
        payload["username"] = "d1masik"
        response = self.client.post(reverse("register"), data=payload)
        self.assertEqual(response.status_code, 400)

    def test_create_user_validate_password(self):
        payload = self.create_user_payload.copy()

        payload["password"] = "kjha"
        response = self.client.post(reverse("register"), data=payload)
        self.assertEqual(response.status_code, 400)
        payload["password"] = "kjhsadfsdfsdfa"
        response = self.client.post(reverse("register"), data=payload)
        self.assertEqual(response.status_code, 400)
        payload["password"] = "kjFsadfsdfsdfa"
        response = self.client.post(reverse("register"), data=payload)
        self.assertEqual(response.status_code, 400)
        payload["password"] = "kjFsadfsdfsdfa!1"
        response = self.client.post(reverse("register"), data=payload)
        self.assertEqual(response.status_code, 201)
