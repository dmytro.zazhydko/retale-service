from django.contrib.auth import get_user_model
from rest_framework.serializers import ModelSerializer
from rest_framework import serializers

from users.validators import MyPasswordValidator

User = get_user_model()


class GetUserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = "__all__"


class CreateUserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ("username", "email", "password")

    def create(self, validated_data):
        user = User(**validated_data)
        user.set_password(validated_data["password"])
        user.save()
        return user

    def validate_email(self, value):
        lower_email = value.lower()
        if User.objects.filter(email__iexact=lower_email).exists():
            raise serializers.ValidationError("User with this email already exists!")
        return lower_email

    def validate_password(self, value):
        password_validator = MyPasswordValidator()
        password_validator.validate(value)
        return value
