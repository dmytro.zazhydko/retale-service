import re
from rest_framework import serializers


class MyPasswordValidator:

    min_length = 8

    def validate(self, password, user=None):
        if len(password) < self.min_length:
            raise serializers.ValidationError(
                "This password must contain at least 8 characters.",
                code="too_short",
            )
        if not any(char.isupper() for char in password):
            raise serializers.ValidationError(
                "This password must contain at least one uppercase letter.",
                code="no_upper",
            )
        # Special chars
        regex = re.compile("[,\+-\.!@#$%^&*()_<>?/\|}{~:\\\\\]\[]")
        if not regex.search(password):
            raise serializers.ValidationError(
                "This password must contain at least one special character.",
                code="no_special_char",
            )
